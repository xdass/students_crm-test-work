from django.contrib import admin
from .models import Group
from students.models import Student


class StudentInline(admin.TabularInline):
    model = Student
    extra = 0


class GroupAdmin(admin.ModelAdmin):
    inlines = (StudentInline, )
    list_display = ('name', 'head_of_group', 'persons_in_group')

    def persons_in_group(self, obj):
        return obj.students.count()
    persons_in_group.short_description = 'Человек в группе'

admin.site.register(Group, GroupAdmin)
