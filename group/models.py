from django.db import models


# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    head_of_group = models.ForeignKey('students.Student', blank=True, related_name='head_of_group',
                                      null=True, on_delete=models.SET_NULL, verbose_name='Староста')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Группы'


class DbReport(models.Model):
    model_name = models.CharField(max_length=100)
    action = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now_add=True)