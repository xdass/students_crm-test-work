from django.conf.urls import url
from .views import GroupList, GroupView, GroupEdit, GroupDelete, GroupCreate


urlpatterns = [
    url(r'^$', GroupList.as_view(), name='groups'),
    url(r'^(?P<pk>\d+)/$', GroupView.as_view(), name='group_info'),
    url(r'^edit/(?P<pk>\d+)/$', GroupEdit.as_view(), name='edit_group'),
    url(r'^delete/(?P<pk>\d+)/$', GroupDelete.as_view(), name='delete_group'),
    url(r'^create/$', GroupCreate.as_view(), name='create_group'),
]