from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, DeleteView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Group
from group.forms import GroupEditForm, GroupNewForm
import logging

logger = logging.getLogger(__name__)


# Create your views here.
class GroupList(ListView):
    model = Group
    template_name = 'group/list.html'
    context_object_name = 'groups'


class GroupView(ListView):
    template_name = 'group/info.html'
    context_object_name = 'group'

    def get_queryset(self):
        group_name = Group.objects.get(id=self.kwargs['pk'])
        students = group_name.students.all()
        return students

    def get_context_data(self, **kwargs):
        context = super(GroupView, self).get_context_data(**kwargs)
        context['group_name'] = Group.objects.get(id=self.kwargs['pk'])
        return context


class GroupCreate(LoginRequiredMixin, CreateView):
    login_url = 'login'
    model = Group
    form_class = GroupNewForm
    template_name = 'group/new.html'
    success_url = reverse_lazy('groups')

    def get_context_data(self, **kwargs):
        context = super(GroupCreate, self).get_context_data(**kwargs)
        context['panel_title'] = 'Новая группа'
        return context

    def form_valid(self, form):
        logger.info('Форма валидна, студент добавлен')
        return super(GroupCreate, self).form_valid(form)


class GroupEdit(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Group
    template_name = 'group/edit.html'
    form_class = GroupEditForm
    success_url = reverse_lazy('groups')

    def get_form_kwargs(self):
        kwargs = super(GroupEdit, self).get_form_kwargs()
        kwargs.update({'id': self.kwargs['pk']})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(GroupEdit, self).get_context_data(**kwargs)
        context['panel_title'] = 'Редактирование группы'
        return context

    def form_valid(self, form):
        return super(GroupEdit, self).form_valid(form)


class GroupDelete(LoginRequiredMixin, DeleteView):
    login_url = 'login'
    model = Group
    success_url = reverse_lazy('groups')

    def get(self, *args, **kwargs):
        messages.success(self.request, 'Группа удалена')
        return self.post(*args, **kwargs)
