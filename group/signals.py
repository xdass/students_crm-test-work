from django.db.models.signals import post_delete
from django.db.models.signals import post_save
from django.dispatch import receiver
from group.models import Group, DbReport


@receiver(post_delete, sender=Group)
def after_group_delete(sender, instance, **kwargs):
    model_name = '{}-{}'.format(instance.__class__.__name__, instance.name)
    report = DbReport.objects.create(model_name=model_name, action='Delete')
    report.save()


@receiver(post_save, sender=Group, dispatch_uid="grp")
def after_group_add(sender, instance, created, **kwargs):
    if created:
        model_name = '{}-{}'.format(instance.__class__.__name__, instance.name)
        report = DbReport.objects.create(model_name=model_name, action='Create')
        report.save()
    else:
        model_name = '{}-{}'.format(instance.__class__.__name__, instance.name)
        report = DbReport.objects.create(model_name=model_name, action='Edit')
        report.save()

