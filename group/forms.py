from django import forms
from django.forms.widgets import RadioSelect
from .models import Group
from students.models import Student


class GroupEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id')
        super(GroupEditForm, self).__init__(*args, **kwargs)
        self.fields['head_of_group'].queryset = self.fields['head_of_group'].queryset.filter(group_id=self.id)

    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'head_of_group': forms.Select(attrs={'class': 'form-control'})
        }
        labels = {
            'name': 'Название',
            'head_of_group': 'Староста'
        }

    # def clean(self):
    #     cleaned_data = super(GroupEditForm, self).clean()
    #     print(cleaned_data)
    #     raise forms.ValidationError('Invalid value', code='invalid')


class GroupNewForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'head_of_group': forms.Select(attrs={'class': 'form-control'})
        }
        labels = {
            'name': 'Название',
            'head_of_group': 'Староста'
        }