from django.core.management.base import BaseCommand, CommandError
from group.models import Group


class Command(BaseCommand):
    help = 'Print all groups and amount of students in this group'

    def handle(self, *args, **options):
        groups = Group.objects.all()
        for group in groups:
            print('{} - человек в группе {}'.format(group, group.students.count()))
