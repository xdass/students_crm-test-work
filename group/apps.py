from django.apps import AppConfig


class GroupConfig(AppConfig):
    name = 'group'
    verbose_name = 'Управление группами'

    def ready(self):
        import group.signals

