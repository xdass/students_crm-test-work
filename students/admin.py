from django.contrib import admin
from .models import Student


class StudentsAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'group')
    search_fields = ['group__name']
    list_filter = ['group__name']

admin.site.register(Student, StudentsAdmin)