from django.conf.urls import url
from .views import StudentCreate, StudentDelete, StudentEdit

urlpatterns = [
        url(r'^create/$', StudentCreate.as_view(), name='create_student'),
        url(r'^edit/(?P<pk>\d+)/$',  StudentEdit.as_view(), name='edit_student'),
        url(r'^delete/(?P<pk>\d+)/$', StudentDelete.as_view(), name='student_delete'),
    ]
