from django.urls import reverse
from django import template
from django.contrib.contenttypes.models import ContentType

register = template.Library()


@register.inclusion_tag('link_tag.html')
def edit_obj(obj, link=None):
    obj_id = obj.id
    content_type = ContentType.objects.get_for_model(obj.__class__)
    template_url = 'admin:{}_{}_change'.format(content_type.app_label, content_type.model)
    url = reverse(template_url, args=(obj_id,))
    return {'obj': url, 'link': link}
