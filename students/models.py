from django.db import models
#from group.models import Group


class Student(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя')
    surname = models.CharField(max_length=100, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=100, verbose_name='Отчество')
    date_of_birth = models.DateField(verbose_name='Дата рождения')
    student_id = models.IntegerField('Номер студенческого')
    group = models.ForeignKey('group.Group', blank=True, null=True, related_name='students', verbose_name='Группа')  # Many-To-One

    def __str__(self):
        return '{} {} {}'.format(self.name, self.surname, self.patronymic)

