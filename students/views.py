from django.contrib import messages
from django.contrib.auth import views
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Student
from group.models import Group
from .forms import CreateStudent


# Create your views here.
class MainPage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(MainPage, self).get_context_data(**kwargs)
        context['students_count'] = Student.objects.count()
        context['groups_count'] = Group.objects.count()
        return context


class LoginUser(views.LoginView):
    template_name = 'login.html'
    success_url = reverse_lazy('home')


class StudentCreate(LoginRequiredMixin, CreateView):
    login_url = 'login'
    #redirect_field_name = 'groups'
    model = Student
    form_class = CreateStudent
    template_name = 'students/new.html'
    success_url = reverse_lazy('groups')

    def get_context_data(self, **kwargs):
        context = super(StudentCreate, self).get_context_data(**kwargs)
        context['panel_title'] = 'Новый студент'
        return context

    def form_valid(self, form):
        form.save()
        return super(StudentCreate, self).form_valid(form)


class StudentEdit(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Student
    template_name = 'students/edit.html'
    form_class = CreateStudent
    success_url = reverse_lazy('groups')

    def get_context_data(self, **kwargs):
        context = super(StudentEdit, self).get_context_data(**kwargs)
        context['panel_title'] = 'Редактирование'
        return context


class StudentDelete(LoginRequiredMixin, DeleteView):
    login_url = 'login'
    model = Student
    success_url = reverse_lazy('groups')

    def get(self, *args, **kwargs):
        messages.success(self.request, 'Студент удален')
        return self.post(*args, **kwargs)
