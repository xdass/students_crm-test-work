from django.test import TestCase, Client
from django.contrib.auth.models import User
from group.models import Group


# Create your tests here.
class UserLogin(TestCase):
    user = 'test_user'
    password = 'test_password'
    email = 'test@test.ru'

    student = {
        'name': 'Васиилй',
        'surname': 'Бочин',
        'patronymic': 'Петрови',
        'date_of_birth': '10.01.1986',
        'student_id': 99999,
        'group': None
    }

    def init_test(self):
        self.client = Client()

    def test_login(self):
        User.objects.create_user(username=self.user, password=self.password, email=self.email)
        result = self.client.post('/login/', {'username': self.user, 'password': self.password}, follow=True)
        self.assertEqual(result.context['user'].username, self.user)

    def test_add_group(self):
        User.objects.create_user(username=self.user, password=self.password, email=self.email)
        self.client.post('/login/', {'username': self.user, 'password': self.password}, follow=True)

        self.client.post('/groups/create/', {'name': 'Тестовая группа'})
        context = self.client.get('/groups/').context['object_list']
        self.assertEqual(context.count(), 1)

        group_id = Group.objects.get(name='Тестовая группа').pk
        self.student['group'] = group_id
        response = self.client.post('/students/create/', self.student)
        self.assertEqual(response.status_code, 302)
