from django import forms
from .models import Student
from django.forms.utils import ErrorList
from django.contrib.auth.forms import AuthenticationForm


class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )


class DivErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()

    def as_divs(self):
        if not self:
            return ''
        return '<div class="errorlist">%s</div>' % ''.join(['<small>%s</small>' % e for e in self])
        #return '<div class="">{}</div>'.format(''.join(['<div class="">{}</div>'.format(e for e in self)]))


class CreateStudent(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CreateStudent, self).__init__(*args, **kwargs)
        self.error_class = DivErrorList
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Student
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'surname': forms.TextInput(attrs={'class': 'form-control'}),
            'patronymic': forms.TextInput(attrs={'class': 'form-control'}),
            'date_of_birth': forms.TextInput(attrs={'class': 'form-control'}),
            'student_id': forms.TextInput(attrs={'class': 'form-control'}),
            'group': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': 'Имя',
            'surname': 'Фамилия',
            'patronymic': 'Отчество',
            'date_of_birth': 'Дата рождения',
            'student_id': 'Номер студенческого',
            'group': 'Группа'
        }

    def as_div(self):
        return self._html_output(
            normal_row=u'<div class=\'group-control\' %(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row=u'<div class="error">%s</div>',
            row_ender='</div>',
            help_text_html=u'<div class="hefp-text">%s</div>',
            errors_on_separate_row=False)

    def clean(self):
        group = self.cleaned_data.get('group')
        student_id = self.cleaned_data.get('student_id')
        if Student.objects.filter(student_id=student_id).exists():
            self.add_error('student_id', 'Такой номер уже есть')
        if group:
            return self.cleaned_data
        else:
            self.add_error('group', 'Выберите группу')
            raise forms.ValidationError('Общая ошибка формы')