from django.conf import settings
from django.template import Context


def add_settings(request):
    return dict(settings=settings)
