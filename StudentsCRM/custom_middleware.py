from datetime import datetime
from django.utils.deprecation import MiddlewareMixin
from django.db import connection
from django.template import Template, Context


class SimpleMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request._request_time = datetime.now()

    def process_response(self, request, response):
        if response['content-type'].startswith('text/html'):
            t = sum([float(q['time']) for q in connection.queries])
            q = len(connection.queries)
            content = response.content.decode()
            template = Template("""<div class='debug-info'>
                <p>Количество запросов: {{ q }}</p>
                <p>Время: {{ t }}</p>
            </div>
            """)
            rendered_template = template.render(Context(dict({'q': q, 't': t})))
            pos = content.find('</body>')
            content = '{}{}{}'.format(content[:pos], rendered_template, content[pos:])
            response.content = content.encode()
        return response

    # def process_template_response(self, request, response):
    #     response_time = request._request_time - datetime.now()
    #     response.context_data['response_time'] = abs(response_time)
    #     return response