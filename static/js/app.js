/**
 * Created by Dass on 08.06.2017.
 */
function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))
}
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i=0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue
}

var csrftoken = getCookie('csrftoken');

// $('.add_group').click(function (e) {
//     e.preventDefault()
//     $('#myModal').modal('show');
//     $('#myModal .modal-body').load('/groups/create');
// });
//
// $('.add_student').click(function (e) {
//     $('#myModal').modal('show');
//     $('#myModal .modal-body').load('/students/create');
// });

// $('#myModal').on('submit', '#formA',function () {
//     $.ajax({
//         url: '/students/create/',
//         type: 'POST',
//         beforeSend: function (xhr, settings) {
//             if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
//                 xhr.setRequestHeader('X-CSRFToken', csrftoken);
//             }
//         },
//         success: function (data) {
//             console.log(data);
//         }
//     });
// });